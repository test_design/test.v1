# Prueba 

## Recursos
- Íconos: https://fontawesome.com/icons
- Framework: https://getbootstrap.com/ v5.2
- Librerías: jquery y libre a aeleccion
- Usar less o sass y/o webpack
- Imágenes: https://picsum.photos/
- Contenido de fantasía: https://www.lipsum.com/
- Git

## Prueba 

Teniendo como base el archivo layout.html
1. Los elemento del layout.html son todos obligatorios, por lo que no se puede eliminar.
2. Se pueden añadir elementos.
3. Se pueden establecer clases e ids.
4. Armar 3 pantallas, copiando el archivo layout.
5. Completar el contenido usando https://www.lipsum.com/ y https://picsum.photos/, también pueden utilizarse imágenes de stock.

Ésto es una prueba, así que hay trampa.

### Pantalla 1: Home
Debe ser una muestra de lo que hace la Empresa, no se debe ver el aside.
Debe contener:
1. Una imagen de presentación.
2. Bloque de título, contenido textual, imagen.
3. Bloque de título, contenido textual, imagen, botón.
4. Bloque de título, contenido.
5. Un slider.

### Pantalla 2: Página de "quiénes somos"
Se deberá ver el aside, deberá contener
1. Bloque de título, contenido textual, imágenes.

### Pantalla 3: Contacto
Deberá contener:
- Mapa
- Domicilio
- Teléfonos
- Formulario de contacto, con los siguientes campos:
  - nombre
  - teléfono
  - e-mail
  - select de área con la que se quiere comunicar
  - textarea del mensaje

## Evaluación
La evaluación se va a hacer sobre:
- Velocidad de entrega.
- Ordenamiento de los directorios.
- Independencia y codependencia.
- Formas de resolver el maquetado.
- Formas de uso de los recursos.
- Diseño e usabilidad.
- Uso de Git.
